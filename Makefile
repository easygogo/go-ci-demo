PROJECT_NAME := "gitlab.com/easygogo/go-ci-demo"
PKG := "$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# 测试覆盖率临界值，高于低于该值则测试不通过
COVER_MAX = 60
# 需要注意awk中的$, 必须是2个
COVER_VAL = $(shell go test ./... -v -coverprofile .coverage.txt | grep ^coverage | awk '{print $$2}' | awk -F. '{print $$1}')

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test ./... -v -coverprofile .coverage.txt

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

coverage: test
	#./scripts/coverage.sh;
	@go tool cover -html=.coverage.txt

check-cover:
	echo "coverage result:" ${COVER_VAL}
	@if [ ${COVER_MAX} -gt ${COVER_VAL} ] ; then \
		echo "coverage insufficient."; \
		exit 1; \
 	else \
 	  	echo "coverage pass"; \
 	  	exit 0; \
 	fi

coverhtml: ## Generate global code coverage report in HTML
	./scripts/coverage.sh html;

dep: ## Get the dependencies
	@go get -v -d ./...
	@go get -u github.com/golang/lint/golint

build: dep ## Build the binary file
	@go build -i -v $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
