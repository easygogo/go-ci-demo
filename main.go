package main

import (
	"fmt"
	"gitlab.com/easygogo/go-ci-demo/util"
)

func main() {
	fmt.Println("start test sum")

	ret := util.Sum(1, 2)

	fmt.Println("1+2 = ", ret)
}
